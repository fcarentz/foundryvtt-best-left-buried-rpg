/**
 * A simple and flexible system for world-building using an arbitrary collection of character and item attributes
 * Author: Atropos
 * Software License: GNU GPLv3
 */

// Import Modules
import { registerCustomHelpers } from "./module/handlebarsHelpers.js";
import { preloadHandlebarsTemplates } from "./module/preloadTemplates.js";
import { _getInitiativeFormula } from "./module/combat.js";

import { SimpleActor } from "./module/actor.js";
import { BLBItemSheet } from "./module/item-sheet1.js";
import { SimpleActorSheet } from "./module/actor-sheet.js";
import { BLBMonsterSheet} from "./module/monster-sheet.js";


/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing Best Left Buried`);


    registerCustomHelpers();


	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	/*
	CONFIG.Combat.initiative = {
	  formula: "1d3 + @init_mod",
      decimals: 0
  };*/
  Combat.prototype._getInitiativeFormula = _getInitiativeFormula;

	// Define custom Entity classes
  CONFIG.Actor.entityClass = SimpleActor;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  //Actors.registerSheet("dnd5e", SimpleActorSheet, { makeDefault: true });
    Actors.registerSheet('blb', SimpleActorSheet, {
        types: ['character'],
        makeDefault: true,
    });
    Actors.registerSheet('blb', BLBMonsterSheet, {
        types: ['monster'],
        makeDefault: true,
    });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("blb", BLBItemSheet, {makeDefault: true});

    await preloadHandlebarsTemplates();
  // Register system settings
  game.settings.register("worldbuilding", "macroShorthand", {
    name: "Shortened Macro Syntax",
    hint: "Enable a shortened macro syntax which allows referencing attributes directly, for example @str instead of @attributes.str.value. Disable this setting if you need the ability to reference the full attribute model, for example @attributes.str.label.",
    scope: "world",
    type: Boolean,
    default: true,
    config: true
  });

  Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return {
      "+": lvalue + rvalue,
      "-": lvalue - rvalue,
      "*": lvalue * rvalue,
      "/": lvalue / rvalue,
      "%": lvalue % rvalue
    }[operator];
  });

  Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });



});
