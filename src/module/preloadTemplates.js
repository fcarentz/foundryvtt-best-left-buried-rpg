export const preloadHandlebarsTemplates = async function () {
    const templatePaths = [
        //Character Sheets
        'systems/bestleftburied/templates/actors/actor-sheet.html',
        'systems/bestleftburied/templates/actors/monster-sheet.html',
        //Actor partials
        //Sheet tabs
        'systems/bestleftburied/templates/actors/partials/gear-tab.html',
        'systems/bestleftburied/templates/actors/partials/features-tab.html',

        //Sheet Parts
        'systems/bestleftburied/templates/actors/partials/equipped-area.html'

    ];
    return loadTemplates(templatePaths);
};
